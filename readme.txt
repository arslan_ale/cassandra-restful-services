This application implemented on NodeJS provides RESTful services to communicate with 
cassandra(http://cassandra.apache.org/) NoSQL database.

*For more details refer to Wiki.*

## installation guide: ##

install node:

### step 1: ###
- nodejs.org
or
- brew install node

### step 2: ###

-open terminal
-goto root dir of application
-run command "npm install"
-run command "node bin/www"